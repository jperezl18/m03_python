#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-05-06 17:31:46
18 Versió: 1

Descripció: El programa llegeix el fitxer dades.csv i i permet a l'usuari modificar 
les dades guardades en ell.
'''

import csv

def carregar_dades():
    try:
        with open('dades.csv', 'r', newline='') as file:
            reader = csv.DictReader(file)
            dades = [row for row in reader]
        print("Dades carregades amb èxit!")
        return dades
    except FileNotFoundError:
        print("Fitxer no trobat.")
        return []

def desar_dades(dades):
    # Eliminar diccionaris amb camps buits
    dades = [row for row in dades if all(row.values())]
    
    if not dades:
        print("No hi ha dades per desar.")
        return
    
    with open('dades.csv', 'w', newline='') as file:
        headers = ['id', 'price', 'brand', 'model', 'year', 'title_status', 'mileage', 'color', 'vin', 'lot', 'state', 'country', 'condition']
        writer = csv.DictWriter(file, fieldnames=headers)
        writer.writeheader()
        writer.writerows(dades)
    print("Dades desades amb èxit!")

def llistar_dades(dades):
    if not dades:
        print("No hi ha dades carregades.")
        return
    headers = dades[0].keys()
    print("\nDades:")
    for row in dades:
        for header in headers:
            print(f"{header}: {row[header]}", end="\t")
        print()

def modificar_dades(dades):
    if not dades:
        print("No hi ha dades carregades.")
        return
    identificador = input("Introdueix l'ID del registre que vols modificar: ")
    for row in dades:
        if row['id'] == identificador:
            print("Registre trobat:")
            print(row)
            for key in row.keys():
                nou_valor = input(f"Introdueix el nou valor per a '{key}' (Enter per mantenir): ")
                if nou_valor:
                    row[key] = nou_valor
            print("Registre modificat amb èxit!")
            return
    print("No s'ha trobat cap registre amb aquest ID.")

def analitzar_dades(dades):
    if not dades:
        print("No hi ha dades carregades.")
        return

    columnes = dades[0].keys()
    estadistics = {}

    # Nombre total de registres
    estadistics['Nombre de registres'] = len(dades)

    for columna in columnes:
        if columna == 'id':
            continue
        elif columna == 'year':
            # Màxim i mínim per year
            estadistics[columna] = {
                'Màxim': max(int(row[columna]) for row in dades),
                'Mínim': min(int(row[columna]) for row in dades)
            }
        elif columna == 'mileage':
            # Màxim, mínim i mitjana per mileage
            estadistics[columna] = {
                'Màxim': max(float(row[columna]) for row in dades),
                'Mínim': min(float(row[columna]) for row in dades),
                'Mitjana': sum(float(row[columna]) for row in dades) / len(dades)
            }
        elif columna == 'price':
            # Màxim, mínim i mitjana per price
            preus = [float(row[columna]) for row in dades]
            estadistics[columna] = {
                'Màxim': max(preus),
                'Mínim': min(preus),
                'Mitjana': sum(preus) / len(dades)
            }
        elif columna != 'lot':  # Ometem la columna 'lot'
            # Nombre de valors únics i valors buits per les altres columnes
            estadistics[columna] = {
                'Valors únics': len(set(row[columna] for row in dades if row[columna] != '')),
                'Valors buits': sum(1 for row in dades if row[columna] == '')
            }

    print("\nEstadístiques:")
    with open('statistics.txt', 'w') as file:
        for columna, stats in estadistics.items():
            if columna == 'Nombre de registres':
                file.write(f"{columna}: {stats}\n")
                continue
            file.write(f"{columna}:\n")
            if isinstance(stats, dict):  # Comprovem si stats és un diccionari
                for key, value in stats.items():
                    file.write(f"\t{key}: {value}\n")
            else:  # Si no és un diccionari, simplement escribim el valor
                file.write(f"\tTotal: {stats}\n")
            file.write('\n')