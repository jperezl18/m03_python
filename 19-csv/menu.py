#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-05-06 17:31:46
18 Versió: 1

Descripció: El programa llegeix el fitxer dades.csv i i permet a l'usuari modificar 
les dades guardades en ell.
'''

import funcions

def menu():
    dades = []
    while True:
        print("\nMenú:")
        print("1. Carregar dades")
        print("2. Llistar dades")
        print("3. Modificar dades")
        print("4. Desar dades")
        print("5. Analitzar dades")
        print("6. Sortir")
        
        opcio = input("Escull una opció (1-6): ")

        match opcio:
            case '1':
                dades = funcions.carregar_dades()
            case '2':
                funcions.llistar_dades(dades)
            case '3':
                funcions.modificar_dades(dades)
            case '4':
                funcions.desar_dades(dades)
            case '5':
                funcions.analitzar_dades(dades)
            case '6':
                desar = input("Vols desar les dades abans de sortir? (s/n): ")
                if desar.lower() == 's':
                    funcions.desar_dades(dades)
                break
            case _:
                print("Opció invàlida. Torna a intentar.")

if __name__ == "__main__":
    menu()