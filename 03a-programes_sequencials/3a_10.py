#1 centímetre són 0.39737 polzades.
POLZADES = 0.39737

while True:
    try:
        centimetres = float(input("Introdueix una qüantitat de centímetres que serà convertida a polzades: "))
    except ValueError:
        print("Error: El número ha de ser enter o racional.")
        continue

    if centimetres >= 0:
        conversio = centimetres * POLZADES
        break
    else:
        print("Error: El número ha de ser positiu.")
        continue

print(centimetres, "cm són", round(conversio, 2), "Polzades.")