while True:
    try:
        base = int(input("Introdueix la base del nombre: "))
    except ValueError:
        print("Error: La base ha de ser un nombre enter.")
        continue

    if base > 0:
        break
    else:
        print("Error: La base ha de ser un nombre positiu major que 0.")
        continue

while True:
    print("Introdueix 3 dígits en base", base, ": ")
    num = input()
    
    try:
        conv = int(num, base)
    except ValueError:
        print("Error: Has d'introduïr dígits de base", base, ".")
        continue
    
    if len(num) != 3:
        print("Error: Has d'introduïr 3 dígits de base", base, ".")
        continue
    else:
        print("El nombre", num, "de base", base, "equival al", conv, "en decimal.")
        break





#n = input("N: ")
#base = int(input("Base: "))

#conv = int(n, base)

#print("Decimal =", conv)