while True:
    try:
        segons = int(input("Introdueix una quantitat exacta de segons:"))
    except ValueError:
        print("Error. Has d'introduïr un nombre sencer.")
        continue

    if segons >= 0:
        hores = segons // 3600
        minuts = (segons % 3600) // 60
        restants = segons % 60
        break
    else:
        print("Error. Has d'introduïr un nombre sencer positiu.")
        continue

print(segons, "Segons equivalen a", hores, "Hores", minuts, "Minuts i", restants, "segons.")

