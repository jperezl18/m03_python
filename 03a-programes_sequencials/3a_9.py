#1 Quilòmetre són 1000 metres.
KM = 1000

while True:
    try:
        metres = float(input("Introdueix una qüantitat de metres que serà convertida a quilòmetres: "))
    except ValueError:
        print("Error: El número ha de ser enter o racional.")
        continue

    if metres >= 0:
        conversio = metres / KM
        break
    else:
        print("Error: El número ha de ser positiu.")
        continue

print(metres, "m són", round(conversio, 2), "Km.")