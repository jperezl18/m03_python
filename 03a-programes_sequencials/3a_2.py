import time

while True:
    try:
        a = int(input("Introdueix un nombre enter 'a': "))
    except ValueError:
        print("Error. 'a' Ha de ser un nombre sencer.")
        continue
    else:
        break

while True:
    try:
        b = int(input("Introdueix un nombre enter 'b': "))
    except ValueError:
        print("Error. 'b' Ha de ser un nombre sencer.")
    else:
        break

print("a =", a, "b =", b)

time.sleep(3)

a ,b = b, a

print("a =", a, "b =", b)







#primera versió:
#print("Introdueix un nombre enter 'a':")
#a = int(input())

#print("Introdueix un nombre enter 'b':")
#b = int(input())

#print("a=", a, "b=", b)

#time.sleep(3)

#a ,b = b, a

#print("a=", a, "b=", b)