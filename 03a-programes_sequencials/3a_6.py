while True:
    try:
        catet1 = float(input("Introdueix la longitud (en m) del primer catet: "))
    except ValueError:
        print("Error: El catet ha de ser un número enter o racional.")
        continue

    if catet1 >= 0:
        break
    else:
        print("Error: El catet ha de ser un número positiu.")
        continue

while True:
    try:
        catet2 = float(input("Introdueix la longitud (en m) del segon catet: "))
    except ValueError:
        print("Error: El catet ha de ser un número enter o racional.")
        continue

    if catet2 >= 0:
        area = (catet1 * catet2 / 2)
        break
    else:
        print("Error: El catet ha de ser un número positiu.")
        continue

print("L'area del triangle és de", round(area, 2), "m2.")