import time

while True:
    try:
        sou1 = float(input("Introdueix el sou del primer treballador: "))
    except ValueError:
        print("Error: El sou ha de ser un nombre enter o racional.")
        continue
    
    if sou1 < 0:
        print("Error: El sou ha de ser positiu.")
        continue
    else:
        break

while True:
    try:
        sou2 = float(input("Introdueix el sou del segon treballador: "))
    except ValueError:
        print("Error: El sou ha de ser un nombre enter o racional.")
        continue
    
    if sou2 < 0:
        print("Error: El sou ha de ser positiu.")
        continue
    else:
        break
        
while True:
    try:
        sou3 = float(input("Introdueix el sou del tercer treballador: "))
    except ValueError:
        print("Error: El sou ha de ser un nombre enter o racional.")
        continue
    
    if sou3 < 0:
        print("Error: El sou ha de ser positiu.")
        continue
    else:
        break

sou1 = sou1 * 1.10
sou2 = sou2 * 1.12
sou3 = sou3 * 1.17

print("El primer treballador rep un increment del 10%. El sou final és:", round(sou1, 2), "€")
time.sleep(3)
print("El segon treballador rep un increment del 12%. El sou final és:", round(sou2, 2), "€")
time.sleep(3)
print("El tercer treballador rep un increment del 17%. El sou final és:", round(sou3, 2), "€")


#print("Introdueix el sou del primer treballador:")
#sou1 = float(input())

#print("Introdueix el sou del segon treballador:")
#sou2 = float(input())

#print("Introdueix el sou del tercer treballador:")
#sou3 = float(input())

#sou1 = sou1 * 1.10
#sou2 = sou2 * 1.12
#sou3 = sou3 * 1.17

#print("El primer treballador rep un increment del 10%. El sou final és:", sou1, "€")
#print("El segon treballador rep un increment del 12%. El sou final és:", sou2, "€")
#print("El tercer treballador rep un increment del 17%. El sou final és:", sou3, "€")