#1 Euro equival a 0.82 dòlars.
DOLARS = 0.82

while True:
    try:
        euros = float(input("Introdueix una qüantitat d'euros que serà convertida a dòlars: "))
    except ValueError:
        print("Error: El número ha de ser enter o racional.")
        continue

    if euros >= 0:
        conversio = euros * DOLARS
        break
    else:
        print("Error: El número ha de ser positiu.")
        continue

print(euros, "€ són", round(conversio, 2), "$.")