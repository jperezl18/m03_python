IMPOSTOS = 0.80

while True:
    try:
        nom = str(input("Introdueix el nom del treballador: "))
    except ValueError:
        print("Error: El nom ha de ser format per lletres.")
        continue
    else:
        break

while True:
    try:
        hores = int(input("Introdueix les hores treballades: "))
    except ValueError:
        print("Error: Les hores han de ser introduïdes en números enters.")
        continue
    
    if hores >= 0:
        break
    else:
        print("Error: Les hores han de ser un número positiu.")
        continue

while True:
    try:
        sou_hora = float(input("Introdueix el seu salari per hora: "))
    except ValueError:
        print("Error: EL salari per hora ha de ser un nombre enter o racional.")
        continue

    if sou_hora >= 0:
        sou_net = sou_hora * hores
        sou_brut = sou_net * IMPOSTOS
        break
    else:
        print("Error: El salari per hora ha de ser un nombre positiu.")
        continue

print("En", nom, "ha treballat", hores, "hores. El seu salari per hora és de", sou_hora, "€. El salari net serà de", round(sou_net, 2), "€ i el seu salari brut serà de", round(sou_brut, 2), "€.")
