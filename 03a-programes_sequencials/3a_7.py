#1 Euro equival a 166 pessetes.
PESSETES = 166

while True:
    try:
        euros = float(input("Introdueix una qüantitat d'euros que serà convertida a pessetes: "))
    except ValueError:
        print("Error: El número ha de ser enter o racional.")
        continue

    if euros >= 0:
        conversio = euros * PESSETES
        break
    else:
        print("Error: El número ha de ser positiu.")
        continue

print(euros, "€ són", round(conversio, 2), "pessetes.")