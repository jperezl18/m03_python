while True:
    try:
        euros = int(input("Introdueix una qüantitat sencera d'euros. Es mostrarà el mínim de bitllets i monedes per assolhir la qüantitat: "))
    except ValueError:
        print("Error: El número d'euros ha de ser natural.")
        continue

    if euros > 0:
        posicio = 0
        bitllets = [200, 100, 50, 20, 10, 5, 2, 1]
        break
    elif euros == 0:
        print("Error: Ets pobre. Introdueix una qüantitat major que 0, si la tens.")
    else:
        print("Error: El número ha de ser positiu.")
        continue

while euros > 0:
    if euros >= bitllets[posicio]:
        print("Bitllet(s) de", bitllets[posicio], ":", euros // bitllets[posicio])
        euros = euros % bitllets[posicio]
        posicio += 1
    else:
        posicio += 1
