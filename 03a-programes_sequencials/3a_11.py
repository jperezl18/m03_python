while True:
    try:
        hores = int(input("Introdueix una qüantitat d'hores: "))
    except ValueError:
        print("Error: Les hores han de ser un nombre natural.")
        continue

    if hores >= 0:
        break
    else:
        print("Error: Les hores han de ser un número positiu.")
        continue

while True:
    try:
        minuts = int(input("Introdueix una qüantitat de minuts: "))
    except ValueError:
        print("Error: Els minuts han de ser un nombre natural.")
        continue

    if minuts >= 0:
        break
    else:
        print("Error: Els minuts han de ser un número positiu.")
        continue

while True:
    try:
        segons = int(input("Introdueix una qüantitat de segons: "))
    except ValueError:
        print("Error: Els segons han de ser un nombre natural.")
        continue

    if segons >= 0:
        break
    else:
        print("Error: Els segons han de ser un número positiu.")
        continue

if minuts < 60 and segons < 60:
    print("El temps ja està normalitzat:", hores, "h", minuts, "m", segons, "s.")
else:
    while segons >= 60:
        minuts = minuts + (segons // 60)
        segons = segons % 60

    while minuts >= 60:
        hores = hores + (minuts // 60)
        minuts = minuts % 60

    print("El temps normalitzat és:", hores, "h", minuts, "m", segons, "s.")

    