permesos = ['0', '1']

while True:
    
    binari = str(input("Introdueix 8 dígits en binari: "))

    if any(x not in permesos for x in binari):
        print("Error: Has d'introduïr dígits binaris.")
        continue
    elif len(binari) != 8:
        print("Error: Has d'escriure 8 dígits binaris.")
        continue
    else:
        posicio = 0
        decimal = 0
        break

while posicio < len(binari):
    if binari[::-1][posicio] == "1":
        decimal = decimal + 2**(posicio)
        posicio += 1
    else:
        posicio += 1

print("El número binari", binari, "equival al", decimal, "en decimal.")
