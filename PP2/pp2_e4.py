#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-01-17 09:21:09
pp2_e4 Versió: 1

Descripció: llegeix quatre noms i els mostra ordenats per ordre alfabètic ascendent.

Especificacions d'entrada:

4 strings.

Joc de proves:
                        Entrada         Sortida
    Execució 1          10              7.5
                        5             
    
    Execució 2          NP              10
                        10                    
    
    Execució 3          NP              l'alumne no s'ha presentat.
                        NP
'''

nom1 = input("Introdueix el primer nom: ")
nom2 = input("Introdueix el segon nom: ")
nom3 = input("Introdueix el tercer nom: ")
nom4 = input("Introdueix el quart nom: ")

