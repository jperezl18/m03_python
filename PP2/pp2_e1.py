#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-01-17 08:10:11
pp2_e1 Versió: 1

Descripció: El programa ha de mostrar per pantalla
aquell, o aquells, dels tres nombres que tingui més dígits iguals al dígit de referència.

Especificacions d'entrada:

3 nombres naturals de 3 dígits i un nombre natural d'un dígit.

Joc de proves:
                        Entrada         Sortida
    Execució 1          244             244
                        342             464
                        464
                        4
    
    Execució 2          777             777
                        707
                        700
                        7
    
    Execució 3          033              033
                        303              303        
                        330              330
                        3
'''

num1 = str(input("Introdueix el primer nombre natural: "))
num2 = str(input("Introdueix el segon nombre natural: "))
num3 = str(input("Introdueix el tercer nombre natural: "))
ref = str(input("Introdueix el dígit de referència: "))

posicio = 0
n_digits = 0
n2_digits = 0
n3_digits = 0

while posicio < len(num1):
    if num1[::-1][posicio] == ref:
        n_digits += 1
        posicio += 1
    else:
        posicio +=1

posicio = 0

while posicio < len(num2):
    if num2[::-1][posicio] == ref:
        n2_digits += 1
        posicio += 1
    else:
        posicio +=1

posicio = 0

while posicio < len(num3):
    if num3[::-1][posicio] == ref:
        n3_digits += 1
        posicio += 1
    else:
        posicio +=1

if n_digits > n2_digits and n_digits > n3_digits:
    print(num1, "té més dígits iguals")
elif n2_digits > n_digits and n2_digits > n3_digits:
    print(num2, "té més dígits iguals")
elif n3_digits > n_digits and n3_digits > n2_digits:
    print(num3, "té més dígits iguals")
elif n_digits == n2_digits and n_digits == n3_digits:
    print(num1, ",", num2, "i", num3, "tenen els mateixos dígits iguals")
elif n_digits == n2_digits:
    print(num1, "i", num2, "tenen més dígits iguals")
elif n_digits == n3_digits:
    print(num1, "i", num3, "tenen més dígits iguals")
elif n2_digits == n3_digits:
    print(num2, "i", num3, "tenen més dígits iguals")
