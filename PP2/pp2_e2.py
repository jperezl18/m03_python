#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-01-17 08:10:11
pp2_e2 Versió: 1

Descripció:  llegeix un nombre sencer positiu que pot tenir entre 1 i 5 xifres. El
programa mostra per pantalla el missatge El nombre X té Y xifres., on Y és el nombre de xifres que té el
nombre. O bé mostra El nombre X té més de cinc xifres.

Especificacions d'entrada:

1 nombre sencer positiu de entre 1 i 5 xifres.

Joc de proves:
                        Entrada         Sortida
    Execució 1          123             3
    
    Execució 2          213124124       El nombre té més de 5 xifres
'''

num = str(input("Introdueix un nombre sencer positiu: "))

xifres = 0

for n in num:
    xifres += 1

if xifres <= 5:
    print("El nombre", num, "té", xifres, "xifres")
else:
    print("El nombre", num, "té més de 5 xifres")

