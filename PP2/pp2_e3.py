#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-01-17 09:09:13
pp2_e3 Versió: 1

Descripció: llegeix els valors de les notes de dos exàmens i mostra per pantalla
la nota mitjana.

Especificacions d'entrada:

2 nombres float entre 0 i 10 o l'string "NP".

Joc de proves:
                        Entrada         Sortida
    Execució 1          10              7.5
                        5             
    
    Execució 2          NP              10
                        10                    
    
    Execució 3          NP              l'alumne no s'ha presentat.
                        NP
'''

nota1 = str(input("Introdueix la nota del primer examen: "))
nota2 = str(input("Introdueix la nota del segon examen: "))

if nota1 == "NP" and nota2 == "NP":
    print("L'Alumne no s'ha presentat a cap examen.")
elif nota1 == "NP":
    print("La nota de l'alumne és", nota2)
elif nota2 == "NP":
    print("La nota de l'alumne és", nota1)
else:
    print("La nota mitjana de l'alumne és", (int(nota1) + int(nota2)) / 2)


    