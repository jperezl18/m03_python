#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-05-22 08:10:45
files-and-folders Versió: 1

Descripció: El programa mostra un menú amb opcions en relació
a fitxers i carpetes del sistema. El menú està al fitxer menu.py.
'''

import os
import shutil
import fnmatch

def mostrar_contingut_carpeta(carpeta):
    """
    Mostra el contingut d'una carpeta, destacant les carpetes amb color blau.

    Args:
        carpeta (str): El camí de la carpeta a mostrar.

    Exceptions:
        FileNotFoundError: Si la carpeta no existeix.
    """
    try:
        for item in os.listdir(carpeta):
            if os.path.isdir(os.path.join(carpeta, item)):
                print(f"\033[34m{item}\033[0m")  # Color blau per a carpetes
            else:
                print(item)
    except FileNotFoundError:
        print("Carpeta no trobada.")

def crear_arxiu_buit(percorregut_arxiu):
    """
    Crea un arxiu buit.

    Args:
        percorregut_arxiu (str): El camí de l'arxiu a crear.
    """
    open(percorregut_arxiu, 'w').close()
    print(f"Arxiu {percorregut_arxiu} creat.")

def canviar_nom_arxiu(nom_antic, nom_nou):
    """
    Canvia el nom d'un arxiu.

    Args:
        nom_antic (str): El nom actual de l'arxiu.
        nom_nou (str): El nou nom de l'arxiu.

    Exceptions:
        FileNotFoundError: Si l'arxiu no existeix.
    """
    try:
        os.rename(nom_antic, nom_nou)
        print(f"Arxiu {nom_antic} renombrat a {nom_nou}.")
    except FileNotFoundError:
        print("Arxiu no trobat.")

def eliminar_arxiu(percorregut_arxiu):
    """
    Elimina un arxiu.

    Args:
        percorregut_arxiu (str): El camí de l'arxiu a eliminar.

    Exceptions:
        FileNotFoundError: Si l'arxiu no existeix.
    """
    try:
        os.remove(percorregut_arxiu)
        print(f"Arxiu {percorregut_arxiu} eliminat.")
    except FileNotFoundError:
        print("Arxiu no trobat.")

def buscar_arxiu_amb_comodins(carpeta, patró):
    """
    Busca arxius en una carpeta que compleixin amb un patró de comodins.

    Args:
        carpeta (str): El camí de la carpeta on buscar.
        patró (str): El patró de comodins per buscar arxius.
    """
    for root, dirs, files in os.walk(carpeta):
        for filename in fnmatch.filter(files, patró):
            print(os.path.join(root, filename))

def buscar_arxius_amb_cadena(carpeta, cadena):
    """
    Busca arxius que continguin una cadena específica.

    Args:
        carpeta (str): El camí de la carpeta on buscar.
        cadena (str): La cadena a buscar dins dels arxius.
    """
    for root, dirs, files in os.walk(carpeta):
        for filename in files:
            file_path = os.path.join(root, filename)
            try:
                with open(file_path, 'r', errors='ignore') as file:
                    if cadena in file.read():
                        print(file_path)
            except (UnicodeDecodeError, PermissionError):
                pass

def crear_carpeta(percorregut_carpeta):
    """
    Crea una nova carpeta.

    Args:
        percorregut_carpeta (str): El camí de la carpeta a crear.
    """
    os.makedirs(percorregut_carpeta, exist_ok=True)
    print(f"Carpeta {percorregut_carpeta} creada.")

def canviar_nom_carpeta(nom_antic, nom_nou):
    """
    Canvia el nom d'una carpeta.

    Args:
        nom_antic (str): El nom actual de la carpeta.
        nom_nou (str): El nou nom de la carpeta.

    Exceptions:
        FileNotFoundError: Si la carpeta no existeix.
    """
    try:
        os.rename(nom_antic, nom_nou)
        print(f"Carpeta {nom_antic} renombrada a {nom_nou}.")
    except FileNotFoundError:
        print("Carpeta no trobada.")

def eliminar_carpeta(percorregut_carpeta):
    """
    Elimina una carpeta i tot el seu contingut.

    Args:
        percorregut_carpeta (str): El camí de la carpeta a eliminar.

    Exceptions:
        FileNotFoundError: Si la carpeta no existeix.
    """
    try:
        shutil.rmtree(percorregut_carpeta)
        print(f"Carpeta {percorregut_carpeta} eliminada.")
    except FileNotFoundError:
        print("Carpeta no trobada.")

def mostrar_permisos(percorregut):
    """
    Mostra els permisos d'un arxiu o carpeta.

    Args:
        percorregut (str): El camí de l'arxiu o carpeta.

    Exceptions:
        FileNotFoundError: Si el camí no existeix.
    """
    try:
        permisos = oct(os.stat(percorregut).st_mode)[-3:]
        print(f"Permisos per {percorregut}: {permisos}")
    except FileNotFoundError:
        print("Camí no trobat.")