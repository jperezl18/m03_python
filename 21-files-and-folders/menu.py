#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-05-22 08:10:24
files-and-folders Versió: 1

Descripció: El programa mostra un menú amb opcions en relació
a fitxers i carpetes del sistema. Les funcions estan al fitxer funcions.py.
'''

from funcions import *

def main():
    """
    Mostra un menú amb opcions per gestionar fitxers i carpetes del sistema.

    L'usuari pot escollir diferents opcions per mostrar el contingut d'una carpeta,
    crear arxius o carpetes, canviar noms, eliminar elements, buscar arxius, i mostrar permisos.
    """
    while True:
        print("\nMenú:")
        print("1. Mostrar el contingut d'una carpeta")
        print("2. Crear un arxiu buit")
        print("3. Canviar el nom d'un arxiu")
        print("4. Eliminar un arxiu")
        print("5. Buscar un arxiu amb comodins")
        print("6. Buscar arxius que contenen una cadena")
        print("7. Crear una carpeta")
        print("8. Canviar el nom d'una carpeta")
        print("9. Eliminar una carpeta")
        print("10. Mostrar permisos d'un arxiu/carpeta")
        print("11. Sortir")
        
        opció = input("Escull una opció: ")
        
        match opció:
            case '1':
                carpeta = input("Introdueix el camí de la carpeta: ")
                mostrar_contingut_carpeta(carpeta)
            case '2':
                percorregut_arxiu = input("Introdueix el camí de l'arxiu: ")
                crear_arxiu_buit(percorregut_arxiu)
            case '3':
                nom_antic = input("Introdueix el nom actual de l'arxiu: ")
                nom_nou = input("Introdueix el nou nom de l'arxiu: ")
                canviar_nom_arxiu(nom_antic, nom_nou)
            case '4':
                percorregut_arxiu = input("Introdueix el camí de l'arxiu: ")
                eliminar_arxiu(percorregut_arxiu)
            case '5':
                carpeta = input("Introdueix el camí de la carpeta: ")
                patró = input("Introdueix el patró de comodins: ")
                buscar_arxiu_amb_comodins(carpeta, patró)
            case '6':
                carpeta = input("Introdueix el camí de la carpeta: ")
                cadena = input("Introdueix la cadena a buscar: ")
                buscar_arxius_amb_cadena(carpeta, cadena)
            case '7':
                percorregut_carpeta = input("Introdueix el camí de la carpeta: ")
                crear_carpeta(percorregut_carpeta)
            case '8':
                nom_antic = input("Introdueix el nom actual de la carpeta: ")
                nom_nou = input("Introdueix el nou nom de la carpeta: ")
                canviar_nom_carpeta(nom_antic, nom_nou)
            case '9':
                percorregut_carpeta = input("Introdueix el camí de la carpeta: ")
                eliminar_carpeta(percorregut_carpeta)
            case '10':
                percorregut = input("Introdueix el camí de l'arxiu/carpeta: ")
                mostrar_permisos(percorregut)
            case '11':
                print("Sortint.")
                break
            case _:
                print("Opció invàlida, si us plau, torna-ho a provar.")

if __name__ == "__main__":
    main()