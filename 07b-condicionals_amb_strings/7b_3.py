#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
7b_3 Versió: 1


Especificacions d'entrada:

Dos cadenes de caràcters.

Joc de proves:          
                        Entrada            Sortida
        Execució 1      ViscaelRealMadrid  Vàlida
                        ViscaelRealMadrid

        Execució 2      ViscaelRealMadrid  Invàlida
                        viscaelrealmadrid   
                         
'''

contrasenya = input("Introdueix una contrasenya: ")

contrasenya_repetida = input("Si us plau, repeteix la contrasenya: ")

if contrasenya == contrasenya_repetida:
    print("Contrasenya vàlida")
else:
    print("Contrasenya invàlida")