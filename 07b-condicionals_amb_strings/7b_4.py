#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
7b_4 Versió: 1


Especificacions d'entrada:

Dos cadenes de caràcters.

Joc de proves:          
                        Entrada            Sortida
        Execució 1      ViscaelRealMadrid  Vàlida
                        ViscaelRealMadrid

        Execució 2      ViscaelRealMadrid  no coincideixen
                        viscaelrealmadrid 

        Execució 3      viscaelrealmadrid  No comença amb majúscula
                        viscaelrealmadrid

        Execució 4      Madrid             Massa curta
                        Madrid
'''

def main():
    contrasenya = input("Introdueix una contrasenya: ")
    contrasenya_repetida = input("Si us plau, repeteix la contrasenya: ")

    if contrasenya != contrasenya_repetida:
        print("Les dos contrasenyes no coincideixen.")
        return

    if len(contrasenya) <= 8:
        print("La contrasenya és massa curta.")
        return

    if not contrasenya[0].isupper():
        print("La contrasenya no comença amb una lletra majúscula.")
        return

    print("Contrasenya vàlida")

if __name__ == "__main__":
    main()