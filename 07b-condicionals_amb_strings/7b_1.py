#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
7b_1 Versió: 1


Especificacions d'entrada:

Dos cadenes de caràcters.

Joc de proves:          
                        Entrada         Sortida
        Execució 1      Jorge           Ana
                        Ana             Jorge

        Execució 2      Pau             Pau
                        Pere            Pere 
'''

nom1 = input("Introdueix el primer nom: ")
nom2 = input("Introdueix el segon nom: ")

noms_ordenats = sorted([nom1, nom2])

print("Els noms ordenats son:")

for nom in noms_ordenats:
    print(nom)

