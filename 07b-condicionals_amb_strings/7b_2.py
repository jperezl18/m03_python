#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
7b_2 Versió: 1


Especificacions d'entrada:

Una lletra.

Joc de proves:          
                        Entrada         Sortida
        Execució 1      A               Majúscula

        Execució 2      a               Minúscula

        Execució 3      !               No és una lletra
                         
'''

lletra = input("Introdueix una lletra: ")

if lletra.isupper():
    print("La lletra és majúscula.")
elif lletra.islower():
    print("La lletra és minúscula.")
else:
    print("El caracter introduït no és una lletra.")