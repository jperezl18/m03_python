#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-04-24 08:48:46
ex04_biblioteca Versió: 1


Descripció: El programa és composa de 3 fitxers: ex04_biblioteca.py, biblioteca_funcions.py i biblioteca.json.
El programa utilitza una estructura de diccionaris per guardar les dades ja que és l'estructura més simple i clara.
'''     

from biblioteca_funcions import llistar_llibres, reservar_llibre, desar_dades, carregar_dades

def main():
    # Aquesta funció mostra el menú d'opcions del programa. En seleccionar una opció, crida les funcions del fitxer biblioteca_funcions.py
    fitxer = 'biblioteca.json'
    biblioteca = carregar_dades(fitxer)

    while True:
        print("\nMenú de la biblioteca:")
        print("1. LListar llibres")
        print("2. Reservar llibre")
        print("7. Sortir")

        opcio = input("\nSelecciona una opció:")

        if opcio == "1":
            llistar_llibres(biblioteca)
        elif opcio == "2":
            reservar_llibre(biblioteca)
        elif opcio == "7":
            print("Guardant dades a la biblioteca...")
            desar_dades(biblioteca, fitxer)
            break
        else:
            print("Opció no vàlida, Si us plau, selecciona una opció vàlida.")
        
if __name__ == "__main__":
    main()