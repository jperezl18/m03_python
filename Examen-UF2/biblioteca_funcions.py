import json

def llistar_llibres(biblioteca):
    #LLista els llibres enmagatzemats a biblioteca.json
    if not biblioteca:
        print("La biblioteca està buida.")
    else:
        print("Llistat de llibres:")
        for llibre in biblioteca['books']:
            print(f"ISBN: {llibre['isbn']}, Títol: {llibre['title']}, Autor: {llibre['author']}, Estat: {llibre['status']}")

def reservar_llibre(biblioteca):
    #Reserva un llibre, només si el seu status és available.
    isbn = input("Introdueix el ISBN del llibre que vols reservar: ")
    for llibre in biblioteca['books']:
        if llibre['isbn'] == isbn:
            if llibre['status'] == 'available':
                llibre['status'] = 'reserved'
                print("Llibre reservat amb éxit.")
            else:
                print("Aquest llibre no està disponible per reservar.")
            return
    print("Llibre no trobat a la biblioteca.")

def desar_dades(biblioteca, fitxer):
    #Guarda tots els cambis realitzats en l'execució del programa al fitxer biblioteca.json
    with open(fitxer, 'w') as f:
        json.dump(biblioteca, f, indent=4)

def carregar_dades(fitxer):
    #Llegeix les dades del fitxer biblioteca.json per mostrar-les al programa 
    try:
        with open(fitxer, 'r') as f:
            return json.load(f)
    except FileNotFoundError:
        return {"books": []}