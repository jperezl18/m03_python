#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-04-24 09:39:24
ex02_cadenes Versió: 1


Descripció: El programa conté 3 funcions: trobar_subcadena, es_palindrom i eliminar_elements_repetits. A la funció main hi han
els seus corresponents test drivers.
'''  

def trobar_subcadena(cadena, subcadena):
    indexes = []
    index = cadena.find(subcadena)
    while index != -1:
        indexes.append(index)
        index = cadena.find(subcadena, index + 1)
    return indexes if indexes else [0]

def es_palindrom(cadena):
    return cadena == cadena[::-1]

def eliminar_elements_repetits(llista):
    return list(set(llista))

def main():
    # A. Test de la funció trobar_subcadena
    print("Test de la funció trobar_subcadena:")
    cadena = "Aquesta és una cadena de prova per trobar subcadenes."
    subcadena1 = "és"
    subcadena2 = "a"
    subcadena3 = "cadena"
    print(f"Cadena: {cadena}")
    print(f"Subcadena '{subcadena1}': {trobar_subcadena(cadena, subcadena1)}")
    print(f"Subcadena '{subcadena2}': {trobar_subcadena(cadena, subcadena3)}")
    print(f"Subcadena '{subcadena3}': {trobar_subcadena(cadena, subcadena3)}")

    #B. Test de la funció es_palindrom
    print("\nTest de la funcio es_palindrom:")
    cadena1 = "radar"
    cadena2 = "palindrom"
    cadena3 = "42024"
    print(f"Cadena '{cadena1}': {es_palindrom(cadena1)}")
    print(f"Cadena '{cadena2}': {es_palindrom(cadena2)}")
    print(f"Cadena '{cadena3}': {es_palindrom(cadena3)}")

    #C. Test de la funció eliminar_elements_repetits
    print("\nTest de la funció eliminar_elements_repetits:")
    llista = [1, 2, 3, 4, 2, 3, 5, 6, 7, 8, 1]
    print(f"Llista original: {llista}")
    print(f"Llista sense elements repetits: {eliminar_elements_repetits(llista)}")

if __name__ == "__main__":
    main()