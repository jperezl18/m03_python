import json

# Función para cargar los datos del archivo JSON de ubicaciones
def cargar_ubicaciones(nombre_archivo):
    with open(nombre_archivo, "r") as archivo:
        return json.load(archivo)

# Función para cargar los datos del archivo JSON de objetos
def cargar_objetos(nombre_archivo):
    with open(nombre_archivo, "r") as archivo:
        return json.load(archivo)

# Función para cargar los datos del archivo JSON de encuentros
def cargar_encuentros(nombre_archivo):
    with open(nombre_archivo, "r") as archivo:
        return json.load(archivo)

# Función para mostrar el menú
def mostrar_menu():
    print("Benvingut a l'illa de Vorthal.")
    print("La teva missió és explorar totes les àrees del nostre país, però tingues compte, pots tenir trobades que no siguin molt amigables.")
    print("Opcions disponibles:")
    for key, ubicacion in ubicaciones.items():
        print(f"{key}: {ubicacion['name']} - {ubicacion['description']}")
    print("Sortir: Leave Vorthal")

# Función para mostrar los ítems disponibles en la ubicación actual
def mostrar_items_disponibles():
    print("Els ítems disponibles en aquesta ubicació són:")
    for key, item in objetos.items():
        print(f"{key}: {item['name']} - {item['description']}")

# Función para mostrar información sobre un encuentro
def mostrar_info_encuentro(encuentro):
    print(encuentro['name'])
    print(encuentro['description'])

# Cargar datos de los archivos JSON
ubicaciones = cargar_ubicaciones("locations.json")
objetos = cargar_objetos("items.json")
encuentros = cargar_encuentros("encounters.json")

# Bucle principal del juego
while True:
    mostrar_menu()
    opcion = input("Escull una àrea per explorar: ").lower()

    if opcion == "sortir":
        print("Adeu! Fi del joc.")
        break
    elif opcion in ubicaciones:
        ubicacion_actual = ubicaciones[opcion]
        print(f"Estàs entrant a {ubicacion_actual['name']}.")
        if "items" in ubicacion_actual:
            mostrar_items_disponibles()
            eleccion_item = input("Escull un ítem per portar amb tu: ").lower()
            if eleccion_item in objetos:
                print(f"Has triat {objetos[eleccion_item]['name']}. {objetos[eleccion_item]['description']}")
            else:
                print("Ítem no vàlid.")
        if "encounters" in ubicacion_actual:
            for encuentro_key in ubicacion_actual["encounters"]:
                encuentro_actual = encuentros[encuentro_key]
                mostrar_info_encuentro(encuentro_actual)
                if encuentro_key == "dragon":
                    accion = input("Pots fer dues coses:\n1. Atacar\n2. Amagar\nEscull què vols fer: ")
                    if accion == "1":
                        print("Has decidit atacar al drac. Això serà una batalla difícil!")
                        # Aquí podries implementar la lògica de combat
                    elif accion == "2":
                        print("Has decidit amagar-te. És una decisió sàvia.")
                    else:
                        print("Opció no vàlida.")
                # Podries afegir més lògica per a altres tipus d'encontres aquí
    else:
        print("Opció incorrecta. Si us plau, escull una àrea vàlida.")
