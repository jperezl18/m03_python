#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-04-16 09:07:23
16_3 Versió: 1
'''

def calculate_vowel_frequency(text):
    """
    Calcula la freqüència de cada vocal en un text.

    Paràmetres:
        text (str): Text d'entrada.

    Retorna:
        dict: Diccionari amb la freqüència de cada vocal.
    """
    vowels = 'aeiouAEIOU'
    vowel_frequency = {vowel: 0 for vowel in vowels}
    
    for char in text:
        if char in vowels:
            vowel_frequency[char.lower()] += 1
    
    return vowel_frequency

def display_histogram(vowel_frequency):
    """
    Mostra un histograma de la freqüència de cada vocal.

    Paràmetres:
        freqüència_vocals (dict): Diccionari amb la freqüència de cada vocal.
    """
    max_frequency = max(vowel_frequency.values())
    
    for vowel, frequency in vowel_frequency.items():
        histogram = '#' * int(frequency / max_frequency * 30)
        print(f"{vowel}: {histogram}")

def main():
    """
    Funció principal que executa el programa.
    """
    text = input("Introdueix el text: ")
    vowel_frequency = calculate_vowel_frequency(text)
    display_histogram(vowel_frequency)

if __name__ == "__main__":
    main()