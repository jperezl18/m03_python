#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-04-16 09:03:50
16_2 Versió: 1
'''

import sys

def generate_histogram(file_name):
    """
    Genera un histograma de freqüència de longituds de paraules en un fitxer de text.

    Paràmetres:
        nom_fitxer (str): Nom del fitxer de text.
    """
    try:
        with open(file_name, 'r') as file:
            text = file.read()
    except FileNotFoundError:
        print("El fitxer no es troba.")
        return
    
    word_lengths = {}
    
    for word in text.split():
        length = len(word)
        if length not in word_lengths:
            word_lengths[length] = 1
        else:
            word_lengths[length] += 1
    
    max_length = max(word_lengths.keys())
    
    for length in range(1, max_length + 1):
        frequency = word_lengths.get(length, 0)
        print(f"{length}: {'#' * frequency}")

def main():
    if len(sys.argv) != 2:
        print("Siusplau, proporciona el nom del fitxer com a argument.")
        return
    
    file_name = sys.argv[1]
    generate_histogram(file_name)

if __name__ == "__main__":
    main()