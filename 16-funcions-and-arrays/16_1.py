#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-04-16 09:03:50
16_1 Versió: 1
'''

import sys

def separate_numbers_and_strings(arguments):
    """
    Separa números i cadenes d'una llista d'arguments.

    Paràmetres:
        arguments (list): Llista d'arguments.

    Retorna:
        tuple: Tupla amb dues llistes, una de números i una altra de cadenes.
    """
    numbers = []
    strings = []
    
    for arg in arguments:
        try:
            value = float(arg)
            numbers.append(value)
        except ValueError:
            strings.append(arg)
    
    return numbers, strings

def main():
    arguments = sys.argv[1:]
    
    if not arguments:
        print("No s'han proporcionat arguments.")
        return
    
    numbers, strings = separate_numbers_and_strings(arguments)
    
    if numbers:
        max_number = max(numbers)
        print(f"El valor màxim dels nombres és: {max_number}")
    else:
        print("No s'han proporcionat nombres.")
    
    if strings:
        max_string = max(strings, key=len)
        print(f"La cadena més llarga és: {max_string}")
    else:
        print("No s'han proporcionat cadenes de text.")

if __name__ == "__main__":
    main()





