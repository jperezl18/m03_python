#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2023-11-22 08:43:17
pp1_e5 Versió: 1


Especificacions d'entrada:

3 números integer majors o iguals que 0.
'''

while True:
    try:
        anys = int(input("Introdueix els anys que has viscut: "))
    except ValueError:
        print("Error: Els anys han de ser un nombre sencer.")
        continue

    if anys >= 0:
        break
    else:
        print("Error: Els anys han de ser un nombre positiu.")
        continue

while True:
    try:
        mesos = int(input("Introdueix els mesos que has viscut: "))
    except ValueError:
        print("Error: Els mesos han de ser un nombre sencer.")
        continue

    if mesos >= 0:
        break
    else:
        print("Error: Els mesos han de ser un nombre positiu.")
        continue

while True:
    try:
        dies = int(input("Introdueix els dies que has viscut (avui no compta): "))
    except ValueError:
        print("Error: Els dies han de ser un nombre sencer.")
        continue

    if dies >= 0:
        total_dies = anys * 365 + mesos * 30 + dies
        print("Has viscut", total_dies, "dies en total.") 
        break
    else:
        print("Error: Els dies han de ser un nombre positiu.")
        continue