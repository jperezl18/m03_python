#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2023-11-22 08:58:15
pp1_e6 Versió: 1


Especificacions d'entrada:

1 número integer major o igual que 0.
'''

while True:
    try:
        total_dies = int(input("Introdueix els dies que has viscut: "))
    except ValueError:
        print("Error: Els dies han de ser un nombre sencer.")
        continue

    if total_dies >= 0:
        anys = total_dies // 365
        total_dies = total_dies % 365
        mesos = total_dies // 30
        dies = total_dies % 30
        print("Has viscut", anys, "anys,", mesos, "mesos i", dies, "dies en total.")
        break
    else:
        print("Error: Els dies han de ser un nombre positiu.")
        continue