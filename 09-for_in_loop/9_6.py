#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
9_6 Versió: 1


Especificacions d'entrada:

Un nombre enter positius.

Joc de proves:          
                        Entrada            Sortida
        Execució 1      6                  1
                                           2
                                           3

        Execució 2      5                  No és un nombre perfecte
                                           
                                                                               
'''

print("Perfect number?")
number = int(input("Enter an integer number: "))
sum = 0 # To store the sum of the dividers.
dividers = "" # To store the actual dividers.

for i in range(1, number):
    if number % i == 0:
        sum += i
        if dividers != "": # If we have added one already, add a comma.
            dividers += ", " + str(i)
        else:
            dividers = str(i)

if sum == number:
    print(f"Number {number} is a perfect number. Its dividers are {dividers}.")
else:
    print(f"Number {number} is not a perfect number.")
