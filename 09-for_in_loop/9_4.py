#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
9_4 Versió: 1


Especificacions d'entrada:

-
                                                                               
'''

print("for loops")

print("Example 1:")
for i in range(0, 5):
    for j in range(i, 3):
        print(i, j)

print("\nExample 2:")
for i in range(0, 5):
    for j in range(i, 3):
        print(i, j)

print("\nExample 3:")
for i in range(0, 5):
    for j in range(0, i):
        print(i, j)
