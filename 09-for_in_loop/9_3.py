#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
9_3 Versió: 1


Especificacions d'entrada:

Un nombre enters positius.

Joc de proves:          
                        Entrada            Sortida
        Execució 1      10                 7
                                           5
                                           3
                                           2

        Execució 2      5                  3
                                           2
                                                                               
'''

print("n prime numbers")
x = int(input("How many prime numbers do you want to generate? "))
numbers_generated = 0

n = 2
while numbers_generated < x:
    i = 2
    is_prime = True
#    while i < n / 2 + 1: # Only need to check the first half of the number.
    for i in range(2, n / 2 + 1): # Only need to check the first half of the number.
        if n % i == 0:
            is_prime = False
#        i += 1
    if is_prime:
        print(f"{n} ",end = '')
        numbers_generated += 1
    n += 1
