#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
9_1 Versió: 1


Especificacions d'entrada:

Dos nombre enters positius.

Joc de proves:          
                        Entrada            Sortida
        Execució 1      6                  6
                        4                  12
                                           18
                                           24

        Execució 2      4                  4
                        3                  8
                                           12

'''

print("First n multiples of m")
m = int(input("Enter the number (m): "))
n = int(input(f"How many multiples of {m} do you want to show (n): "))
multiple = m

for i in range(1,n + 1):
    print(f"{m} * {i} = {multiple}")
    multiple += m
