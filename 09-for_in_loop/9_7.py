#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
9_7 Versió: 1


Especificacions d'entrada:

Un nombre enters positius.

Joc de proves:          
                        Entrada            Sortida
        Execució 1      10                 7
                                           5
                                           3
                                           2

        Execució 2      5                  3
                                           2
                                                                               
'''

print("Perfect numbers")
numbers_to_find = int(input("How many perfect numbers do you want me to look for? "))

numbers_found = 0
number = 6 # We know 6 is the first perfect number, so start here.

while numbers_found < numbers_to_find:
    sum = 0 # To store the sum of the dividers.
    dividers = "" # To store the actual dividers.

    for i in range(1, number):
        if number % i == 0:
            sum += i
            if dividers != "": # If we have added one already, add a comma.
                dividers += ", " + str(i)
            else:
                dividers = str(i)

    if sum == number:
        print(f"Number {number} is a perfect number. Its dividers are {dividers}.")
        numbers_found += 1
    number += 1
