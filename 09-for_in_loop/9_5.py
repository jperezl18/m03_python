#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-13 17:31:46
9_5 Versió: 1


Especificacions d'entrada:

Un nombre enter positius.

Joc de proves:          
                        Entrada            Sortida
        Execució 1      10                 2
                                           1

        Execució 2      123123             6
                                           12
                                                                               
'''

print("Number of digits in a number")
number = str(input("Enter an integer number: "))
number_of_digits = 0
sum = 0

for d in number:
    number_of_digits += 1
    sum += int(d)
print(f"Number {number} has {number_of_digits} digits " \
      f"and those digits add up to {sum}.")
