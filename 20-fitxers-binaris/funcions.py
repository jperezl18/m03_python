#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-05-14 09:31:46
fitxers-binaris Versió: 1

Descripció: El programa mostra un menú el qual permet xifrar un fitxer .txt a binari o
desxifrar un fitxer .bin. El menú és troba al fitxer fitxers-binaris.py.
'''

import os

def text_a_binari(text):
    """
    Converteix una cadena de text a la seva representació binària.

    Args:
        text (str): La cadena de text a convertir.

    Returns:
        str: La representació binària de la cadena de text.
    """
    return ''.join(format(ord(c), '08b') for c in text)

def binari_a_text(binari):
    """
    Converteix una cadena binària a text.

    Args:
        binari (str): La cadena binària a convertir.

    Returns:
        str: La representació en text de la cadena binària.
    """
    text = ""
    for i in range(0, len(binari), 8):
        byte = binari[i:i+8]
        text += chr(int(byte, 2))
    return text

def xifrar_fitxer():
    """
    Llegeix un fitxer de text, el converteix a binari i guarda el resultat en un fitxer .bin.

    Demana a l'usuari el nom del fitxer de text, el llegeix, el transforma a dades binàries,
    i guarda el text xifrat en un fitxer binari amb la mateixa base de nom però amb extensió .bin.
    """
    nom_fitxer = input("Introdueix el nom del fitxer de text a xifrar (amb extensió .txt): ")
    if not os.path.exists(nom_fitxer):
        print("El fitxer no existeix.")
        return
    
    with open(nom_fitxer, 'r', encoding='utf-8') as fitxer:
        contingut = fitxer.read()

    contingut_binari = text_a_binari(contingut)
    nom_fitxer_xifrat = nom_fitxer.split('.')[0] + '.bin'
    
    with open(nom_fitxer_xifrat, 'wb') as fitxer_binari:
        fitxer_binari.write(contingut_binari.encode('utf-8'))
    
    print(f"El fitxer s'ha xifrat i guardat com {nom_fitxer_xifrat}.")

def desxifrar_fitxer():
    """
    Llegeix un fitxer binari, el converteix a text i guarda el resultat en un fitxer .txt.

    Demana a l'usuari el nom del fitxer binari, el llegeix, el transforma a text estàndard,
    mostra aquest text per pantalla, i guarda el text desxifrat en un fitxer de text amb la mateixa
    base de nom però amb l'extensió '_desxifrat.txt'.
    """
    nom_fitxer = input("Introdueix el nom del fitxer binari a desxifrar (amb extensió .bin): ")
    if not os.path.exists(nom_fitxer):
        print("El fitxer no existeix.")
        return
    
    with open(nom_fitxer, 'rb') as fitxer_binari:
        contingut_binari = fitxer_binari.read().decode('utf-8')

    contingut_text = binari_a_text(contingut_binari)
    print("Contingut del fitxer desxifrat:")
    print(contingut_text)

    nom_fitxer_desxifrat = nom_fitxer.split('.')[0] + '_desxifrat.txt'
    with open(nom_fitxer_desxifrat, 'w', encoding='utf-8') as fitxer:
        fitxer.write(contingut_text)
    
    print(f"El fitxer s'ha desxifrat i guardat com {nom_fitxer_desxifrat}.")