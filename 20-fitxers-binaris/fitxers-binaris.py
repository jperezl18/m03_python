#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-05-14 09:31:46
fitxers-binaris Versió: 1

Descripció: El programa mostra un menú el qual permet xifrar un fitxer .txt a binari o
desxifrar un fitxer .bin. Les funcions és troben al fitxer funcions.py.
'''

from funcions import xifrar_fitxer, desxifrar_fitxer

def menu():
    """
    Mostra un menú amb les opcions disponibles per xifrar i desxifrar fitxers, o sortir del programa.

    L'usuari pot seleccionar una opció introduint el número corresponent.
    """
    while True:
        print("\nMenú:")
        print("1. Xifrar fitxer de text")
        print("2. Desxifrar fitxer binari")
        print("3. Sortir")
        opcio = input("Selecciona una opció (1, 2, o 3): ")

        match opcio:
            case '1':
                xifrar_fitxer()
            case '2':
                desxifrar_fitxer()
            case '3':
                print("Sortint del programa.")
                break
            case _:
                print("Opció no vàlida. Torna-ho a intentar.")

if __name__ == "__main__":
    menu()