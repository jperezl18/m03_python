#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-03-26 17:31:46
17 Versió: 1
'''

import json

def load_data(filename):
    with open(filename, 'r') as file:
        users = json.load(file)
    return users

def quicksort(arr):
    if len(arr) <= 1:
        return arr
    pivot = arr[len(arr) // 2][1]
    left = [x for x in arr if x[1] > pivot]
    middle = [x for x in arr if x[1] == pivot]
    right = [x for x in arr if x[1] < pivot]
    return quicksort(left) + middle + quicksort(right)

def merge(left, right):
    result = []
    while left and right:
        if left[0][0] < right[0][0]:
            result.append(left.pop(0))
        else:
            result.append(right.pop(0))
    return result + left + right

def mergesort(arr):
    if len(arr) <= 1:
        return arr
    mid = len(arr) // 2
    left = arr[:mid]
    right = arr[mid:]
    return merge(mergesort(left), mergesort(right))

def calculate_influence_score(users):
    influence_scores = []
    for user, data in users.items():
        num_friends = len(data['friends'])
        num_posts = len(data['posts'])
        avg_rating = sum(int(post[1]) for post in data['posts']) / num_posts if num_posts > 0 else 0
        influence_scores.append((user, num_friends * num_posts * avg_rating))

    influence_scores = quicksort(influence_scores)
    
    # Handle ties using mergesort alphabetically by user ID
    def handle_ties(scores):
        if len(scores) <= 1:
            return scores
        pivot = scores[len(scores) // 2][1]
        left = [x for x in scores if x[1] > pivot]
        middle = [x for x in scores if x[1] == pivot]
        right = [x for x in scores if x[1] < pivot]
        return handle_ties(left) + middle + handle_ties(right)
    
    influence_scores = handle_ties(influence_scores)
    return influence_scores[::-1]  # Reverse the sorted list to get descending order

def write_output(influence_scores, output_filename):
    with open(output_filename, 'w') as file:
        for user, score in influence_scores:
            file.write(f'{user}: {score}\n')

if __name__ == "__main__":
    users_data = load_data('user_data.json')
    influence_scores = calculate_influence_score(users_data)
    write_output(influence_scores, 'influence_scores.txt')