#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2023-11-15 08:41:56
7_3 Versió: 1

Descripció: Aques programa demana un número de convidats d'una boda a l'usuari
i quàntes persones vol assignar a cada taula. En cas de que sobrin convidats, només hi haurà
una taula amb un nombre de convidats diferent. El nombre de persones per taula no podrà ser més gran que el nombre de convidats.

Especificacions d'entrada: 1 número integer major que 0 pels convidats.
1 número integer major que 0 i menor o igual que el nombre de convidats per les persones per taula.


Joc de proves:
                 Entrada             Sortida
Execució 1       487                 40 Taules de 12 persones
                 12                  1 Taula de 7 persones     

Execució 2       756                 63 Taules de 12 persones
                 12

Execució 3       487                 28 Taules de 17 persones
                 17                  1 Taula de 11 persona

Execució 4       487                 27 Taules de 18 persones
                 18                  1 Taula de 1 persona
'''            

while True:
    try:
        convidats = int(input("Introdueix el nombre de convidats: "))
    except ValueError:
        print("Error: Els convidats han de ser un nombre enter.")
        continue

    if convidats > 0:
        break
    elif convidats == 0:
        print("Error: No tens amics ni família?")
        continue
    else:
        print("Error: Els convidats han de ser un nombre positiu.")
        continue

while True:
    try:
        persones_taula = int(input("Introdueix el nombre de convidats per taula: "))
    except ValueError:
        print("Error: Els convidats per taula han de ser un nombre enter.")

    if persones_taula > 0 and persones_taula <= convidats:
        taules = convidats // persones_taula
        sobrants = convidats % persones_taula
        break
    else:
        print("Error: Els convidats per taula han de ser un nombre positiu i menor que el nombre de convitas.")
        continue

if sobrants != 0:
    print("Necessitaràs", taules, "taules de", persones_taula, "persones i 1 taula de", sobrants, "persones.")
else:
    print("Necessitaràs", taules, "de", persones_taula, "persones.")