#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2023-12-14 10:40:42
7_1 Versió: 2


Especificacions d'entrada:

Un número sencer.

Joc de proves:          
                        Entrada         Sortida
        Execució 1      -4              4.0
        Execució 2      6.27            6.27    
'''

while True:
    try:
        num = float(input("Introdueix un nombre: "))
    except ValueError:
        print("Error: Has d'introduïr un nombre sencer.")
        continue

    print("El valor absolut és", abs(num))
    break