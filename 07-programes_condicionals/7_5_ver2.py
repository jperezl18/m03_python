#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-01-10 08:57:16
7_5 Versió: 2

Descripció: Resolució d'equacions de segon grau.

Especificacions d'entrada:

3 nombres float diferents de 0.

Joc de proves:
                        Entrada         Sortida
    Execució 1          1               No te solució
                        1
                        1
    
    Execució 2          1               X = -4.0
                        4
                        4
    
    Execució 3          2               X1 = 2.0
                        -6.0            X2 = 1.0
                        4
'''

import math

print("Aquest programa resol equacions de segon grau del tipus ax² + bx + c = 0.")

while True:
    try:
        a = float(input("Introdueix el nombre a: "))
    except ValueError:
        print("Error: Has d'introduïr un nombre real.")
        continue

    else:
        break

while True:
    try:
        b = float(input("Introdueix el nombre b: "))
    except ValueError:
        print("Error: Has d'introduïr un nombre real.")
        continue

    else:
        break

while True:
    try:
        c = float(input("Introdueix el nombre c: "))
    except ValueError:
        print("Error: Has d'introduïr un nombre real.")
        continue

    else:
        break

if a != 0 and b != 0 and c != 0:
    discriminant = b**2 - 4 * a * c

    if discriminant < 0:
        print("L'equacio no té solució.")
    elif discriminant == 0:
        x1 = -b + math.sqrt(b**2 - 4 * a * c) / 2
        print("L'equació té una solució: X =", x1)
    else:
        x1 = (-b + math.sqrt(b**2 - 4 * a * c)) / (2 * a)
        x2 = (-b - math.sqrt(b**2 - 4 * a * c)) / (2 * a)
        print("l'equació té 2 solucions: X1 =", x1, "X2 =", x2)
elif a == 0:
    x1 = c / b
    print("Equació de primer grau. X =", x1)
elif b == 0:
    
