#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2023-12-16 17:47:18
7_2 Versió: 2


Especificacions d'entrada:

Un número sencer entre 0 i 7 (ambdós inclosos).

Joc de proves:          
                        Entrada         Sortida
        Execució 1      0               diumenge
        Execució 2      1               dilluns
        Execució 3      2               dimarts
        Execució 4      3               dimecres
        Execució 5      4               dijous
        Execució 6      5               divendres
        Execució 7      6               dissabte
        Execució 8      7               diumenge  
'''

while True:
    try:
        dia = int(input("Introdueix un número de dia de la setmana (1-7 o 0-6): "))
    except ValueError:
        print("Error: El dia ha de ser un nombre sencer.")
        continue
    
    if dia > 7 or dia < 0:
        print("Error: El dia ha de ser un nombre entre 0 i 7 (ambdós inclosos).")
        continue
    else:
        break

match dia:
    case 0:
        print("El dia", dia, "és diumenge.")
    case 1:
        print("El dia", dia, "és dilluns.")
    case 2:
        print("El dia", dia, "és dimarts.")
    case 3:
        print("El dia", dia, "és dimecres.")
    case 4:
        print("El dia", dia, "és dijous.")
    case 5:
        print("El dia", dia, "és divendres.")
    case 6:
        print("El dia", dia, "és dissabte.")
    case 7:
        print("El dia", dia, "és diumenge.")