#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-01-09 08:22:50
13_1 Versió: 1

Descripció: Programa que a. Diu si una persona és major d'edat o no.
b. Llegeix una quantitat d’hores, minuts i segons, i diu els segons totals.
c. Passa de segons al format ‘HH:MM:SS’.
d. Diu quants dies té un any.
e. Diu quants dies té un mes.
f. Rep la part numèrica d'un NIF i retorna la lletra.
g. Rep un NIF complet i el valida.
h. Diu si una cadena és un nombre sencer.
i. Diu si una cadena és un float o no.


                                                           
                                            
'''


# funcio que llegeix una quantitat d’hores, minuts i segons, i diu els segons totals
import sys
hora = 5
min = 45
seg = 33
def segons(hora,min,seg):
    return (seg + min*60 + hora*3600)
print(segons(hora,min,seg)) 

# funcio que Passa de segons al format ‘HH:MM:SS’
segons2 = 65665
def temps(segons2):
    h=segons2//3600
    segons2=segons2%3600
    m=segons2//60
    s=segons2%60
    return (h,m,s)
print(temps(segons2)) 

# funcio que Diu quants dies té un any
any = 1
def dies(any):
    temps = any*365
    return(temps)
print(dies(any))

# funcio que Diu quants dies té un mes
mes = 1
def dia(mes):
    time = mes*30
    return(time)
print(dia(mes))

# funcio que Rep la part numèrica d'un NIF i retorna la lletra.
dni="21765712"
lletres="TRWAGMYFPDXBNJZSQVHLCKE"

def dnicomp(dni,lletres):
    dninum = int(dni)
    residu = dninum % 23
    lletra = lletres[residu]
    dnifinal = dni + lletra
    return(dnifinal)
print(dnicomp(dni,lletres))

# funcio que Rep un NIF complet i el valida.
dni2="21765712F"

def dnival(dni2,lletres):
    dninum = int(dni2[0:8])
    residu = dninum % 23
    lletra = lletres[residu]
    if lletra == dni2[8]:
        return(True)
    else:
        return(False)
print(dnival(dni2,lletres))

# funcio que Diu si una cadena és un nombre sencer.
enter = "543"
comp_int = enter.isnumeric()
print(comp_int)

# funcio que Diu si una cadena és un float o no.
decimal = "543"
comp_float = decimal.isdecimal()
print(comp_float)