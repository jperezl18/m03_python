#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-01-09 08:22:50
13_2 Versió: 1

Descripció: Programa que a. Diu si una persona és major d'edat o no.
b. Llegeix una quantitat d’hores, minuts i segons, i diu els segons totals.
c. Passa de segons al format ‘HH:MM:SS’.
d. Diu quants dies té un any.
e. Diu quants dies té un mes.
f. Rep la part numèrica d'un NIF i retorna la lletra.
g. Rep un NIF complet i el valida.
h. Diu si una cadena és un nombre sencer.
i. Diu si una cadena és un float o no.


                                                           
                                            
'''
# Primer definim la funcio per passar de hexadecimal a decimal

def hexadecimal(valorhex):
    valordec = 0

# agafarem el primer valor del numero hexadecimal i el multiplicarem per 16,
# en cas que sigui a,b,c,d,e o f primer el passarem a numero. Si es el segon valor
# no caldra multiplicar-lo per 16
    
    for i in range (2):
        if valorhex[i] == "a":
            valordec = valordec + 10
        elif valorhex[i] == "b":
            valordec = valordec + 11
        elif valorhex[i] == "c":
            valordec = valordec + 12
        elif valorhex[i] == "d":
            valordec = valordec + 13
        elif valorhex[i] == "e":
            valordec = valordec + 14
        elif valorhex[i] == "f":
            valordec = valordec + 15
        else:
            valordec = valordec + int(valorhex[i])
        if i == 0:
            valordec = valordec*16
    return(valordec)

# Introduim la frase a desencriptar i fem un for de la meitat de la seva llargada

frase = str(input("Introdueix una frase per a desencriptar: "))
llarg = int(len(frase) / 2)
frase_final = ""
for i in range (llarg):

# Agafem el primer i el segon caracter, posteriorment el tercer i el quart...

    valorhex = frase[(i*2):(i*2+2)]

# Amb la funcio passem els caracters a decimal i atorguem una lletra al valor

    nova_lletra = chr(hexadecimal(valorhex))
    frase_final = frase_final + nova_lletra
print(frase_final)
