#!/usr/bin/env python
# -*-coding:utf-8 -*-
'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2023-11-09 10:25:15
4a_2 Versió: 1


Especificacions d'entrada:

3 números integer majors o iguals que 0.

Joc de proves:          
                        Entrada         Sortida
        Execució 1      1               3h 1m 3s
                        120             
                        63              
'''

while True:
    try:
        hores = int(input("Introdueix una qüantitat d'hores: "))
    except ValueError:
        print("Error: Les hores han de ser un nombre natural.")
        continue

    if hores >= 0:
        break
    else:
        print("Error: Les hores han de ser un número positiu.")
        continue

while True:
    try:
        minuts = int(input("Introdueix una qüantitat de minuts: "))
    except ValueError:
        print("Error: Els minuts han de ser un nombre natural.")
        continue

    if minuts >= 0:
        break
    else:
        print("Error: Els minuts han de ser un número positiu.")
        continue

while True:
    try:
        segons = int(input("Introdueix una qüantitat de segons: "))
    except ValueError:
        print("Error: Els segons han de ser un nombre natural.")
        continue

    if segons >= 0:
        break
    else:
        print("Error: Els segons han de ser un número positiu.")
        continue

if minuts < 60 and segons < 60:
    print("El temps ja està normalitzat:", hores, "h", minuts, "m", segons, "s.")
else:
    while segons >= 60:
        minuts = minuts + (segons // 60)
        segons = segons % 60

    while minuts >= 60:
        hores = hores + (minuts // 60)
        minuts = minuts % 60

    print("El temps normalitzat és:", hores, "h", minuts, "m", segons, "s.")