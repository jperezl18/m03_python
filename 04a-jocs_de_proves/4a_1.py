#!/usr/bin/env python
# -*-coding:utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2023-11-13 18:02:25
4a_1 Versió: 1

Descripció: Normalització del temps en hores minuts i segons.

Especificacions d'entrada:

3 nombres integer majors o iguals a 0.

Joc de proves:
                        Entrada         Sortida
    Execució 1          1               1,2,4
                        2
                        3
    
    Execució 2          1               1,3,0
                        2
                        59
'''

hores = int(input("Introdueix una qüantitat d'hores: "))
minuts = int(input("Introdueix una qüantitat de minuts: "))
segons = int(input("Introdueix una qüantitat de segons: "))

segons = segons + 1
segons_normals = segons % 60
minuts = minuts + segons // 60
minuts_normals = minuts % 60
hores_normals = hores + minuts // 60

print(hores_normals, "h", minuts_normals, "m", segons_normals, "s.")