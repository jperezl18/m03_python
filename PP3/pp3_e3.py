#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-08 10:55:14
pp3_e3 Versió: 1

Descripció: Calcula el factorial d'un nombre sencer, primer fent servir un bucle while i
seguidament fent servir un bucle for…in.

Especificacions d'entrada:

1 nombre natural.

Joc de proves:
                        Entrada         Sortida
    Execució 1          20              2432902008176640000
                                   
    
    Execució 2          5               120                                    
    
'''

num = int(input("Introdueix un nombre natural: "))
original = num

factorial = 1
i = 1

while (i <= num):
    factorial = factorial * i
    i = i + 1

print("El factorial de", original, "amb el bucle while es:", factorial)

num = original

factorial2 = 1

for y in range(1,num+1):
    factorial2 = factorial2 * y

print("El factorial de", original, "amb el bucle for es:", factorial2)