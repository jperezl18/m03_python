#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-08 10:55:14
pp3_e5 Versió: 1

Descripció: Programa que rep un nombre sencer com a argument, i el mostra invertit. Per exemple,
si l'argument és 123456789, la sortida ha de ser 987654321.

Especificacions d'entrada:

1 nombre natural.

Joc de proves:
                        Entrada         Sortida
    Execució 1          20              02
                                   
    
    Execució 2          1231233         3321321                                    
    
'''

import sys

n = str(sys.argv[1])

invertida = n[::-1]

print(invertida)