#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Institut Escola del Treball de Barcelona
Administració de Sistemes Informàtics en Xarxa
Curs 2023-24
Autor: Jorge D. Pérez López
Data: 2024-02-08 10:02:59
pp3_e1 Versió: 1

Descripció: Genera els primers n elements de la seqüència de Fibonacci, on n és un
nombre sencer que passem al nostre programa com a argument.

Especificacions d'entrada:

1 nombre natural que és passarà com a argument.

Joc de proves:
                        Entrada         Sortida
    Execució 1          1               0, 1
                                     
    
    Execució 2          10              0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55                 
'''

import sys

n = int(sys.argv[1])

ultim = 0
penultim = 1
i = 1

if n == 0:
    print(ultim)
elif n == 1:
    print(ultim)
    print(penultim)
elif n == 2:
    print(ultim)
    print(penultim)
    print(penultim)
elif n > 2:
    print(ultim)
    print(penultim)
    while i < n:
        i += 1
        seguent = ultim + penultim
        print(seguent)
        ultim = penultim
        penultim = seguent
        